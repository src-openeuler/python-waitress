%global _empty_manifest_terminate_build 0
Name:		python-waitress
Version:	3.0.2
Release:	1
Summary:	Waitress WSGI server
License:	ZPLv2.1
URL:		https://github.com/Pylons/waitress
Source:         %{url}/archive/v%{version}/waitress-%{version}.tar.gz
BuildArch:	noarch

Requires:	python3-Sphinx
Requires:	python3-docutils
Requires:	python3-pylons-sphinx-themes
Requires:	python3-pytest
Requires:	python3-pytest-cover
Requires:	python3-coverage

%description
Waitress is a production-quality pure-Python WSGI server with very acceptable
performance. It has no dependencies except ones which live in the Python
standard library. It runs on CPython on Unix and Windows under Python 3.7+. It
is also known to run on PyPy 3 (version 3.7 compatible python) on UNIX. It
supports HTTP/1.0 and HTTP/1.1.

%package -n python3-waitress
Summary:	Waitress WSGI server
Provides:	python-waitress
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-waitress
Waitress is a production-quality pure-Python WSGI server with very acceptable
performance. It has no dependencies except ones which live in the Python
standard library. It runs on CPython on Unix and Windows under Python 3.7+. It
is also known to run on PyPy 3 (version 3.7 compatible python) on UNIX. It
supports HTTP/1.0 and HTTP/1.1.

%package help
Summary:	Development documents and examples for waitress
Provides:	python3-waitress-doc
%description help
Waitress is a production-quality pure-Python WSGI server with very acceptable
performance. It has no dependencies except ones which live in the Python
standard library. It runs on CPython on Unix and Windows under Python 3.7+. It
is also known to run on PyPy 3 (version 3.7 compatible python) on UNIX. It
supports HTTP/1.0 and HTTP/1.1.

%prep
%autosetup -n waitress-%{version} -p1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-waitress -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Mar 12 2025 Dongxing Wang <dongxing.wang_a@thundersoft.com> - 3.0.2-1
- Update package to version 3.0.2
  Don't strip value when inserting into WSGI environ
  Add tests to make sure we don't strip non-RFC7230 whitespace from header
  Assume socket is not connected when passed to wasyncore.dispatcher
  Drop Python 3.8 and add Python 3.13

* Wed Oct 30 2024 liningjie <liningjie@xfusion.com> - 3.0.0-3
- Fix CVE-2024-49769

* Tue Oct 29 2024 liningjie <liningjie@xfusion.com> - 3.0.0-2
- Fix CVE-2024-49768

* Thu Jun 06 2024 liuzhilin <liuzhilin@kylinos.cn> - 3.0.0-1
- Upgrade package to version 3.0.0
- Fix a bug that would appear on macOS whereby if we accept() a socket that is already gone, setting socket options would fail and take down the server.
- Fixed testing of vendored asyncore code to not rely on particular naming for errno's.

* Wed Sep 28 2022 guozhengxin <guozhengxin@kylinos.cn> - 2.1.2-1
- Upgrade package to version 2.1.2

* Mon Apr 18 2022 Shinwell_Hu <micromotive@qq.com> - 2.0.0-3
- Backport from 2.1.1 to fix CVE-2022-24761

* Thu Mar 3 2022 zhaoshuang <zhaoshuang@uniontech.com> - 2.0.0-2
- remove some unnecessary buildrequires

* Thu Jul 08 2021 suoxiaocong <suoxiaocong@kylinos.com> - 2.0.0-1
- update package to 2.0.0

* Sat Aug 22 2020 tianwei <tianwei12@huawei.com> - 1.4.4-2
- delete python2

* Thu Jul 16 2020 jixinjie <jixinjie@huawei.com> - 1.4.4-1
- update package to 1.4.4

* Thu Feb 13 2020 Ruijun Ge <geruijun@huawei.com> - 1.1.0-5
- init package
